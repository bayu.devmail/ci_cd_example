const { default: axios } = require("axios")

describe('The router', () => {
    test('The get route /', async () => {
        const url = 'http://127.0.0.1:3000/';

        const res = await axios.get(url)

        expect(res).toBeTruthy()

        expect(res.status).toBe(200)
    })

    test('The get route /teacher', async () => {
        const url = 'http://127.0.0.1:3000/teacher';

        const res = await axios.get(url)

        expect(res).toBeTruthy()

        expect(res.status).toBe(200)
    })

})